package aviatickets.businessLogic.supportingModels;

import aviatickets.model.Airport;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by 1 on 18.07.2016.
 */
public class SearchModel {

    private Airport airportFrom = new Airport("");
    private Airport airportTo = new Airport("");
    private Date dateTime = Calendar.getInstance().getTime();
    private Date backDateTime = Calendar.getInstance().getTime();

    public Airport getAirportFrom() {
        return airportFrom;
    }

    public void setAirportFrom(Airport airportFrom) {
        this.airportFrom = airportFrom;
    }

    public Airport getAirportTo() {
        return airportTo;
    }

    public void setAirportTo(Airport airportTo) {
        this.airportTo = airportTo;
    }

    public Date getDateTime() {
        return dateTime;
    }

    public void setDateTime(Date dateTime) {
        this.dateTime = dateTime;
    }

    public String getDateTimeString(boolean to){
        DateFormat df = new SimpleDateFormat("dd.MM.yyyy");
        Date date;
        if (to){
            date = dateTime;
        }
        else {
            date = backDateTime;
        }

        return df.format(date);
    }

    public Date getBackDateTime() {
        return backDateTime;
    }

    public void setBackDateTime(Date backDateTime) {
        this.backDateTime = backDateTime;
    }
}
