/* Created by 1 on 20.07.2016. */

package aviatickets.businessLogic;

import aviatickets.model.Airport;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

public class Converter {

    public static boolean emailIsValid(String email){
        // valid 146%
        if((email.indexOf("@")>=0)&&(email.length()>=3)) {
            return true;
        } else {
            return false;
        }
    }

    public static Airport airportNameToAirportConvert(String name){
        ArrayList<Airport> airports = Controller.instance().getAirports();
        if (airports.size()>0){
            for (Airport a :
                    airports) {
                if (a.getName().equals(name)){
                    return a;
                }
            }
        }
        return null;
    }

    public static Date stringToDateConvert(String s) {

        SimpleDateFormat format = new SimpleDateFormat("dd.MM.yyyy");
        Date parse = new Date();
        try {
            parse = format.parse(s);
        } catch (Exception e) {
            return Calendar.getInstance().getTime();
        }
        return parse;
    }



}
