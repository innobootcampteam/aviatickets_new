/* Created by 1 on 17.07.2016. */

package aviatickets.businessLogic;

import aviatickets.RouteAlgorythms.RouteBuilder;
import aviatickets.businessLogic.supportingModels.SearchModel;
import aviatickets.dataAccess.AirportCollection;
import aviatickets.dataAccess.FlightCollection;
import aviatickets.model.Airport;
import aviatickets.model.Route;
import aviatickets.model.User;

import java.util.ArrayList;
import java.util.Date;

public class Controller {

    private static final String pathToFiles  = "resources/flights";
    private static Controller _controller;
    public static final String appName =
            " $$$$$$\\             $$\\           $$\\                  $$\\     \n" +
            "$$  __$$\\            \\__|          $$ |                 $$ |    \n" +
            "$$ /  $$ |$$\\    $$\\ $$\\  $$$$$$$\\ $$ |  $$\\  $$$$$$\\ $$$$$$\\   \n" +
            "$$$$$$$$ |\\$$\\  $$  |$$ |$$  _____|$$ | $$  |$$  __$$\\\\_$$  _|  \n" +
            "$$  __$$ | \\$$\\$$  / $$ |$$ /      $$$$$$  / $$$$$$$$ | $$ |    \n" +
            "$$ |  $$ |  \\$$$  /  $$ |$$ |      $$  _$$<  $$   ____| $$ |$$\\ \n" +
            "$$ |  $$ |   \\$  /   $$ |\\$$$$$$$\\ $$ | \\$$\\ \\$$$$$$$\\  \\$$$$  |\n" +
            "\\__|  \\__|    \\_/    \\__| \\_______|\\__|  \\__| \\_______|  \\____/ \n" +
            "                                                                \n" +
            "                                                                \n" +
            "                                                                \n";
    public static final String appDesc = "Avicket is an app for searching and booking flights. \n"
    		+ "To use it, you need to know airport codes - please use the Info section for this. \n"
    		+ "You can also search for two-way flights, find a route between locations and optimize \nyour choice by money or time criteria. "
    		+ "We hope you will like our app, and we wish \nyou a pleasant flight.\n\n  Main menu";
    // Guys, no more comments in Russian please - I get krakozyabras :(
    private SearchModel searchModel = new SearchModel();
    private boolean isNeedBack = false;
    private boolean isFirstTicketSelected = true;
    private boolean sortTime = true;
    private SearchModel backSearchModel = new SearchModel();
    private Converter converter = new Converter();

    private User user = new User();
    //private ArrayList<Flight> allFlights = new ArrayList<>();
    //private ArrayList<Airport> airports = new ArrayList<>();
    private ArrayList<Route> routes = new ArrayList<>();

    private Controller(){
        FlightCollection.init(pathToFiles);
    }

    public static Controller instance(){
        if (_controller == null){
            _controller = new Controller();
        }
        return _controller;
    }

    public SearchModel getSearchModel() {
        return searchModel;
    }

    public boolean isNeedBack() {
        return isNeedBack;
    }

    public void setNeedBack(boolean needBack) {
        isNeedBack = needBack;
    }

    /*public ArrayList<Flight> getAllFlights() {
        //return allFlights;
        FlightCollection.searchFlight()
    }*/

    public ArrayList<Airport> getAirports() {
        //return airports;
        return AirportCollection.getAirports();
    }

    public boolean emailIsValid(String email){
        return converter.emailIsValid(email);
    }

    public Airport getAirport(String name){
        try {

            return converter.airportNameToAirportConvert(name);
        }
        catch (Exception e){
            return null;
        }
    }

    public Date stringToDateConverter(String s) {
        //try {
            return converter.stringToDateConvert(s);
        /*} catch (ParseException e) {
            e.printStackTrace();
            return null;
        }*/
    }

    public ArrayList<Route> getRoutes() {
        //if (routes == null || routes.size()==0 ||searchAgain){
            RouteBuilder routeBuilder = new RouteBuilder();
            routes.clear();
            ArrayList<Route> route = routeBuilder.findRoute(searchModel, sortTime);
            routes.addAll(route);
        //}
        return routes;
    }


    public User getUser() {
        return user;
    }

    public boolean isFirstTicketSelected() {
        return isFirstTicketSelected;
    }

    public void setFirstTicketSelected(boolean firstTicketSelected) {
        isFirstTicketSelected = firstTicketSelected;
    }

    public SearchModel getBackSearchModel() {
        return backSearchModel;
    }

    public void createSecondSearchModel(){
        if (isNeedBack){
            Airport airportTo = searchModel.getAirportTo();
            Airport airportFrom = searchModel.getAirportFrom();
            searchModel.setAirportFrom(airportTo);
            searchModel.setAirportTo(airportFrom);
            searchModel.setDateTime(searchModel.getBackDateTime());
            _controller.setFirstTicketSelected(false);
        }
    }

    public boolean isSortTime() {
        return sortTime;
    }

    public void setSortTime(boolean sortTime) {
        this.sortTime = sortTime;
    }
}
