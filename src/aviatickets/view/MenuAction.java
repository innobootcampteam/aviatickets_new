package aviatickets.view;

/**
 * Created by 1 on 18.07.2016.
 */
public class MenuAction {
    private String value;
    private String description;
    private IMethod action;

    public MenuAction(String value, String description, IMethod action) {
        this.value = value;
        this.description = description;
        this.action = action;
    }

    public String getValue() {
        return value;
    }

    public String getDescription() {
        return description;
    }

    public IMethod getAction() {
        return action;
    }
}
