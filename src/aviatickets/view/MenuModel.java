package aviatickets.view;

import java.text.ParseException;
import java.util.ArrayList;

/**
 * Created by 1 on 18.07.2016.
 */
public class MenuModel {
    private String description;
    private ArrayList<MenuAction> menuActions;

    public MenuModel(String description, ArrayList<MenuAction> actions) {
        this.description = description;
        this.menuActions = actions;
    }

    public String getDescription() {
        return description;
    }

    public ArrayList<MenuAction> getMenuActions() {
        return menuActions;
    }


    public void execute(String value) {
        for (MenuAction menuAction:
             menuActions) {
            if (menuAction.getValue().equals(value)){
                menuAction.getAction().Action();
                return;
            }
        }
    }

    public void show(){
        if (menuActions.size() == 0) return;
        System.out.println();
        System.out.println(description);
        for (MenuAction action :
                menuActions) {
            System.out.println(action.getValue() + ". " + action.getDescription());
        }
    }
}
