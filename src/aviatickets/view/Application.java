package aviatickets.view;

import aviatickets.businessLogic.Controller;
import aviatickets.businessLogic.Converter;
import aviatickets.businessLogic.supportingModels.SearchModel;
import aviatickets.model.*;

import java.util.ArrayList;
import java.util.Date;
import java.util.Scanner;

/**
 * Created by 1 on 18.07.2016.
 */
public abstract class Application {

    private static final int lineOnPage = 15;

    private static Controller _controller = Controller.instance();
    private static Scanner _scanner = new Scanner(System.in);
    private static int RoutesPageNumber = 0;

    private Application() {
    }

    public static void execute(){
        // FlightCollection.init(null);
        mainMenu();
    }
    
    private static void mainMenu(){
    	
        ArrayList<MenuAction> menuActions = new ArrayList<>();
        menuActions.add(new MenuAction("1", "Search", () -> searchMenu()));
        menuActions.add(new MenuAction("2", "Info", () -> infoMenu()));
        //menuActions.add(new MenuAction("9", "Test dataAccess", () -> aviatickets.dataAccess.daTester.launch()));
        menuActions.add(new MenuAction("0", "Exit", () -> System.exit(0)));
        MenuModel menuModel = new MenuModel(_controller.appName + _controller.appDesc + "\n", menuActions);
        showMenu(menuModel);
        
    }

    private static void infoMenu() {
        String list = "List of airports: ";
        for (Airport airport:
                _controller.getAirports()) {
            list = list + "\n    " + airport.getName();
        }
        ArrayList<MenuAction> menuActions = new ArrayList<>();
        menuActions.add(new MenuAction("0", "Back", () -> mainMenu()));
        MenuModel menuModel = new MenuModel(list, menuActions);
        showMenu(menuModel);
    }

    private static void searchMenu(){
    	
        String needBack = "Yes";
        SearchModel searchModel = _controller.getSearchModel();
        if (!_controller.isNeedBack()){
            needBack = "No";
        }
        ArrayList<MenuAction> menuActions = new ArrayList<>();
        menuActions.add(new MenuAction("1", "Departure back is needed: " + needBack, () -> {
            _controller.setNeedBack(!_controller.isNeedBack());
            searchMenu();
        }));
        menuActions.add(new MenuAction("2", "From: "
                + _controller.getSearchModel().getAirportFrom().getName(), () -> {
                    while (true){
                        String result = showInput("Write airport name.");
                        if (result.equals("")){
                            result = "IAC";
                        }
                        Airport airport = _controller.getAirport(result);
                        if (airport!=null){
                            searchModel.setAirportFrom(new Airport(result));
                            break;
                        }

                        System.out.println("Error! We can't find this airport.");
                    }
                    searchMenu();
        }));
        menuActions.add(new MenuAction("3", "To: "
                + _controller.getSearchModel().getAirportTo().getName(), () -> {
                    while (true){
                        String result = showInput("Write airport name.");
                        if (result.equals("")){
                            result = "TDM";
                        }
                        Airport airport = _controller.getAirport(result);
                        if (airport!=null){
                            searchModel.setAirportTo(new Airport(result));
                            break;
                        }
                        System.out.println("Error! We can't find this airport.");
                    }
                    searchMenu();
        }));
        menuActions.add(new MenuAction("4", "Date of departure: " + _controller.getSearchModel().getDateTimeString(true), () -> {
                    while (true){
                        String result = showInput("Write date in format dd.mm.yyyy");
                        Date date = _controller.stringToDateConverter(result);
                        if (date!=null){
                            searchModel.setDateTime(date);
                            break;
                        }
                        System.out.println("Error! We can't understand you.");
                    }
                    searchMenu();
        }));

        MenuModel menuModel = new MenuModel("Search parameters: ", new ArrayList<>(menuActions));
        if (_controller.isNeedBack()){
            menuModel.getMenuActions().add(new MenuAction("5", "Date of departure back: " +
                    _controller.getSearchModel().getDateTimeString(false), () -> {
                while (true){
                    String result = showInput("Write date in format dd.mm.yyyy");
                    Date date = _controller.stringToDateConverter(result);
                    if (date!=null){
                        _controller.getSearchModel().setBackDateTime(date);
                        break;
                    }
                    System.out.println("Error! We can't understand you.");
                }
                searchMenu();
            }));
            menuModel.getMenuActions().add(new MenuAction("6", "Search", () -> {
                RoutesPageNumber = 0;
                searchResultMenu(null);
            }));
        }
        else {
            menuModel.getMenuActions().add(new MenuAction("5", "Search", () -> {
                RoutesPageNumber = 0;
                searchResultMenu(null);
            }));
        }
        menuModel.getMenuActions().add(new MenuAction("0", "Back", () -> mainMenu()));
        showMenu(menuModel);
        
    }

    private static void searchResultMenu(ArrayList<Route> routes) {

        String sort = "Cost";
        if (_controller.isSortTime()){
            sort = "Time";
        }
        if (routes == null){
            routes = _controller.getRoutes();
        }
        String routesStrings = "";
        int i1 = lineOnPage * RoutesPageNumber;
        for (int i = i1; i < i1 + lineOnPage && i<routes.size(); i++) {
            routesStrings = routesStrings + (i+1) + ". Route " + routes.get(i).getFromTo() + ", transfers: " +
                    (routes.get(i).getReservationsCount() - 1) + ", " + routes.get(i).getDatesAndCost() +"\n";
        }

        ArrayList<MenuAction> menuActions = new ArrayList<>();
        ArrayList<Route> finalRoutes = routes;
        menuActions.add(new MenuAction("1", "Sort by: " + sort, () -> {
            _controller.setSortTime(!_controller.isSortTime());
            searchResultMenu(null);
        }));
        menuActions.add(new MenuAction("2", "Previous page", () -> {
            if (RoutesPageNumber>0){
                RoutesPageNumber--;
            }
            searchResultMenu(finalRoutes);
        }));
        menuActions.add(new MenuAction("3", "Next page", () -> {
            if (RoutesPageNumber<finalRoutes.size()/lineOnPage+1){
                RoutesPageNumber++;
            }
            searchResultMenu(finalRoutes);
        }));
        menuActions.add(new MenuAction("4", "Choose", () -> {
            String s = showInput("Write the number of route to select it.");
            selectedRouteMenu(Integer.valueOf(s), finalRoutes);
        }));
        menuActions.add(new MenuAction("0", "Back", () -> mainMenu()));
        MenuModel menuModel = new MenuModel(routesStrings, menuActions);
        showMenu(menuModel);
    }

    private static void selectedRouteMenu(int selectedNum, ArrayList<Route> routes){

        Ticket ticket = new Ticket();
        Route route = routes.get(selectedNum - 1);
        Double total = 0.0;
        System.out.println("    Detail information");
        System.out.println("DATE AND TIME\t    FLIGHT\tDEP\tARR\tPRICE");
        for (int i = 0; i < route.getReservationsCount(); i++) {
            Reservation reservation = route.getReservation(i);
            total+= reservation.getPrice();
            System.out.println(reservation.getDateTimeString(false) + "\t" + reservation.getFlightNumber() + "\t" +
                    reservation.getAirportFrom().getName() + "\t" + reservation.getAirportTo().getName() + "\t" + reservation.getPrice());
        }
        System.out.print("Total cost: " + total);



        ArrayList<MenuAction> menuActions = new ArrayList<>();
        menuActions.add(new MenuAction("1", "Buy", () -> {
            _controller.getUser().addTicket(ticket);
            if (_controller.isNeedBack() && _controller.isFirstTicketSelected()){
                _controller.createSecondSearchModel();
                searchResultMenu(null);
            }
            else {
                buyMenu();
            }
        }));
        menuActions.add(new MenuAction("0", "Back", () -> searchResultMenu(routes)));
        MenuModel menuModel = new MenuModel("", menuActions);
        showMenu(menuModel);
    }

    private static void buyMenu(){

        String paid = "-";
        User user = _controller.getUser();
        if (user.isPaid()){
            paid = "+";
        }
        ArrayList<MenuAction> menuActions = new ArrayList<>();
        menuActions.add(new MenuAction("1", "First name: " + user.getFirstName(), () -> {
            String s = showInput("Enter your first name: ");
            user.setFirstName(s);
            buyMenu();
        }));
        menuActions.add(new MenuAction("2", "Middle name: " + user.getMiddleName(), () -> {
            String s = showInput("Enter your middle name: ");
            user.setMiddleName(s);
            buyMenu();
        }));
        menuActions.add(new MenuAction("3", "Last name: " + user.getLastName(), () -> {
            String s = showInput("Enter your last name: ");
            user.setLastName(s);
            buyMenu();
        }));
        menuActions.add(new MenuAction("4", "Email: " + user.getEmail(), () -> {
            String s = showInput("Enter your email: ");
            if (Converter.emailIsValid(s)){
                user.setEmail(s);
            }
            else {
                System.out.println("Invalid email!");
            }
            buyMenu();
        }));
        menuActions.add(new MenuAction("5", "Pay " + paid, () -> {
            for (int i = 0; i < user.getTicketsCount(); i++) {
                user.getTicket(i).setPaid(true);
            }
            buyMenu();
        }));
        menuActions.add(new MenuAction("6", "Buy", () -> {
            boolean flag = true;
            for (int i = 0; i < user.getTicketsCount(); i++) {
                if (!user.getTicket(i).isPaid()){
                    flag=false;
                }
            }
            if (user.isEverythingAllright()){
                if (flag){
                    System.out.println("Thank you for buying!");
                    System.exit(0);
                }
                else{
                    System.out.println("Error! You should pay for the ticket.");
                    buyMenu();
                }
            }
            else {
                System.out.println("Error! You should enter all information.");
                buyMenu();
            }
        }));
        menuActions.add(new MenuAction("0", "Back", () -> mainMenu()));
        MenuModel menuModel = new MenuModel("", menuActions);
        showMenu(menuModel);
    }
    
    private static void showMenu(MenuModel menuModel){
    	
        while (true){
            menuModel.show();
            String s = _scanner.nextLine();
            menuModel.execute(s);
        }
        
    }

    private static String showInput(String s){
    	
        System.out.print("\n"+s +"\n> ");
        return  _scanner.nextLine();
        
    }
    
}
