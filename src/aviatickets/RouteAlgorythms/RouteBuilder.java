package aviatickets.RouteAlgorythms;

import aviatickets.businessLogic.supportingModels.SearchModel;
import aviatickets.dataAccess.FlightCollection;
import aviatickets.model.Flight;
import aviatickets.model.Reservation;
import aviatickets.model.Route;


import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

// 20.07.2016. Finally I decided to write some useful comments.
// class RouteBuilder is used to find and optimize routes by given criteria

public class RouteBuilder 
{
	@SuppressWarnings("deprecation")
	
	//This method finds routes. Obviously.
	//WARNING!!! Loads of real Hindu code ahead!
	public static ArrayList<Route> findRoute (SearchModel sm, boolean preferredOptimizationType) 
	//preferredOptimizationType: true - time, false - f..ing money ^^
	{
		ArrayList<Flight> targetFlights = new ArrayList<Flight>();
		ArrayList<ArrayList<Flight>> levelTwo=new ArrayList<ArrayList<Flight>>();
		ArrayList<ArrayList<Flight>> levelTwoOptimized=new ArrayList<ArrayList<Flight>>();
		ArrayList<Route> r = new ArrayList<Route>();
		ArrayList<ArrayList<ArrayList<Flight>>> levelThree=new ArrayList<ArrayList<ArrayList<Flight>>>();
		ArrayList<ArrayList<ArrayList<Flight>>> levelThreeOptimized=new ArrayList<ArrayList<ArrayList<Flight>>>();
		ArrayList<ArrayList<Flight>> levelThreeTemp = new ArrayList<ArrayList<Flight>>();
		//I hope I won't get shot for this.
		
		//STEP 1 - straight route
		
		//This part finds flights and removes all the odd ones
		targetFlights = FlightCollection.searchFlight(false, sm.getAirportFrom(), sm.getDateTime());
		for (int i=0; i<targetFlights.size(); i++)
		{
			if(!targetFlights.get(i).getAirportTo().getName().equals(sm.getAirportTo().getName()))
			{
				targetFlights.remove(i);
				i--;
				// Bad bad bad very bad! It feels like trouble T_T
			}
		}
		// Probably sm.getDateTime() wasn't the best decision. I really don't get what the hell happens in the search model.
		// I should probably have written null. Hope tests will show.
		
		// This parts constructs and sorts routes
		if (!targetFlights.isEmpty())
		{
			//targetFlights=optimize(targetFlights, preferredOptimizationType);
			ArrayList<Reservation> reservations = new ArrayList<Reservation>();
			ArrayList<Route> bestRoutes = new ArrayList<Route>();
			for (int i=0; i<targetFlights.size(); i++)
			{
				reservations.add(new Reservation(targetFlights.get(i)));
				bestRoutes.add(new Route());
			}
			for (int i=0; i<targetFlights.size(); i++)
			{
				bestRoutes.get(i).addReservation(reservations.get(i));
			}
			// I'm not sure if two loops were necessary. Still, I feel it's safer not to mess with size of ArrayLists.
			// Not that I don't mess with it though.
			optimizationSort(bestRoutes, preferredOptimizationType);
			return bestRoutes;
		}
		
		//STEP 2 - one medium
		else
		{
			targetFlights = FlightCollection.searchFlight(false, sm.getAirportFrom(), sm.getDateTime());
			
			for(int i=0; i<targetFlights.size(); i++)
			{
				
				targetFlights.get(i).getDateTo().setHours(targetFlights.get(i).getDateTo().getHours()+2);
				levelTwo.add(FlightCollection.searchFlight(false, targetFlights.get(i).getAirportTo(), 
						targetFlights.get(i).getDateTo() ));
				targetFlights.get(i).getDateTo().setHours(targetFlights.get(i).getDateTo().getHours()-2);
				//Another thing a sane programmer wouldn't probably do. Gross, but close to inevitable.
				//That's how you get lost in your own code =.=
				//Thing I want to do is to add 2 hours between flights, and leave Flights unchanged.
			}
			// This part removes odd flights
			for (int i=0; i<levelTwo.size(); i++)
			{
				for (int j=0; j<levelTwo.get(i).size(); j++)
				{
					if(!levelTwo.get(i).get(j).getAirportTo().getName().equals(sm.getAirportTo().getName()))
					{
						levelTwo.get(i).remove(j);
						j--;
					}
						
				}
			}
			// sorting flights
			for (int i=0; i<levelTwo.size(); i++)
			{
				ArrayList<Flight> temp = optimize(levelTwo.get(i), preferredOptimizationType);
				levelTwoOptimized.add(temp);			
			}
			//constructing routes
			for (int i=0; i<targetFlights.size(); i++)
			{
			
				for (int j=0; j<levelTwoOptimized.get(i).size(); j++)
				{
					Route temp = new Route();
					temp.addReservation(new Reservation (targetFlights.get(i)));
					temp.addReservation(new Reservation (levelTwo.get(i).get(j)));
					r.add(temp);
				}
			}
			// Here I assume that all records are in the same order I added them.
		}
		//Optimizes routes if we have any
		if (!r.isEmpty())
		{
			optimizationSort(r, preferredOptimizationType);
			return r;
		}
		
		//STEP 3 - two mediums
		targetFlights = FlightCollection.searchFlight(false, sm.getAirportFrom(), sm.getDateTime());
		levelTwo.clear();
		
		//Adding appropriate second and third flights
		for(int i=0; i<targetFlights.size(); i++)
		{
			
			targetFlights.get(i).getDateTo().setHours(targetFlights.get(i).getDateTo().getHours()+2);
			levelTwo.add(FlightCollection.searchFlight(false, targetFlights.get(i).getAirportTo(), 
					targetFlights.get(i).getDateTo() ));
			targetFlights.get(i).getDateTo().setHours(targetFlights.get(i).getDateTo().getHours()-2);
		}
		
		for (int i=0; i<targetFlights.size(); i++)
		{
			for (int j=0; j<levelTwo.get(i).size(); j++)
			{
				levelTwo.get(i).get(j).getDateTo().setHours(levelTwo.get(i).get(j).getDateTo().getHours()+2);
				levelThreeTemp.add(FlightCollection.searchFlight
						(false, levelTwo.get(i).get(j).getAirportTo(), levelTwo.get(i).get(j).getDateTo()));
				levelTwo.get(i).get(j).getDateTo().setHours(levelTwo.get(i).get(j).getDateTo().getHours()-2);
				
			}
			levelThree.add(levelThreeTemp);
			levelThreeTemp.clear();
		}
		
		//Removing inappropriate flights
		for (int i=0; i<levelThree.size(); i++)
		{
			for (int j=0; j<levelThree.get(i).size(); j++)
			{
				for (int k=0; k<levelThree.get(i).get(j).size(); k++)
				{
					if(!levelThree.get(i).get(j).get(k).getAirportFrom().getName().equals((levelTwo.get(i).get(j).getAirportTo().getName())) ||
							!levelThree.get(i).get(j).get(k).getAirportTo().getName().equals((sm.getAirportTo().getName())))
					{
						levelThree.get(i).get(j).remove(k);
						k--;
					}
				}
			}
		}
		// Whoa, we're getting a true pharaoh pyramid!
		// That's what happens when I try to write my own implementations =.=
		
		// Flight optimization
		for (int i=0; i<levelThree.size(); i++)
		{
			for (int j=0; j<levelThree.get(i).size(); j++)
			{
				ArrayList<Flight> temp = optimize(levelThree.get(i).get(j), preferredOptimizationType);
				levelThreeTemp.add(temp);
			}
			levelThreeOptimized.add(levelThreeTemp);	
		}
		
		// Creating a route
		for (int i=0; i<targetFlights.size(); i++)
		{
			for (int j=0; j<levelTwo.get(i).size(); j++)
			{
				if (!levelThree.get(i).isEmpty()){
					for (int k=0; k<levelThree.get(i).get(j).size(); k++)
					{
						Route temp = new Route();
						temp.addReservation(new Reservation(targetFlights.get(i)));
						temp.addReservation(new Reservation(levelTwo.get(i).get(j)));
						temp.addReservation(new Reservation(levelThree.get(i).get(j).get(k)));
						r.add(temp);
					}
				}
			}
		}
		
		//Optimizing a route
		if (!r.isEmpty())
		{
			optimizationSort(r, preferredOptimizationType);
			return r;
		}
		//If needed, it's possible to add more steps, but I think that's enough.
		//Furthermore I would have to implement an existing algorithm (breadth-first search), and I have trouble with that.
		return new ArrayList<Route>();
	}

	private static void optimizationSort(ArrayList<Route> r, boolean preferredOptimizationType) 
	{
		for (int i=0; i<r.size(); i++)
		{
			for (int j=0; j<r.size(); j++)
			{
				if (preferredOptimizationType)
				{
					if (compareRoutesByTime(r.get(i),r.get(j)))
					{
						Collections.swap (r, i, j);
					}
				}
				else
				{
					if (compareRoutesByMoney(r.get(i),r.get(j)))
					{
						Collections.swap (r, i, j);
					}
				}
			}	
		}
	}

	private static ArrayList<Flight> optimize(ArrayList<Flight> targetFlights, boolean preferredOptimizationType) 
	{
		
		for (int i=0; i<targetFlights.size(); i++)
		{
			for (int j=0; j<targetFlights.size(); j++)
			{
				if (preferredOptimizationType)
				{
					if (compareFlightsByTime(targetFlights.get(i), targetFlights.get(j)))
					{
						Collections.swap(targetFlights, i, j);
					}
				}
				else
				{
					if (compareFlightsByMoney(targetFlights.get(i), targetFlights.get(j)))
					{
						Collections.swap(targetFlights, i, j);
					}
				}
			}
		}
		return targetFlights;
	}
	
	private static boolean compareFlightsByTime (Flight f1, Flight f2)
	{
		int time1 = (int)(f1.getDateTo().getTime()-f1.getDateFrom().getTime());
		int time2 = (int)(f2.getDateTo().getTime()-f1.getDateFrom().getTime());
		if (time1<time2)
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	
	private static boolean compareFlightsByMoney (Flight f1, Flight f2)
	{
		if (f1.getPrice()<f2.getPrice())
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	
	private static boolean compareRoutesByTime (Route r1, Route r2)
	{
		int time1 = (int) (r1.getReservation(r1.getReservationsCount()-1).getDateTimeTo().getTime()-
				r1.getReservation(0).getDateTimeFrom().getTime());
		int time2 = (int) (r2.getReservation(r2.getReservationsCount()-1).getDateTimeTo().getTime()-
				r2.getReservation(0).getDateTimeFrom().getTime());
		if (time1<time2)
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	
	private static boolean compareRoutesByMoney (Route r1, Route r2)
	{
		double sum1=0, sum2=0;
		for (int i=0; i<r1.getReservationsCount(); i++)
		{
			sum1+=r1.getReservation(i).getPrice();
		}
		for (int i=0; i<r2.getReservationsCount(); i++)
		{
			sum2+=r2.getReservation(i).getPrice();
		}
		if (sum1<sum2)
		{
			return true;
		}
		else
		{
			return false;
		}
	}

}
