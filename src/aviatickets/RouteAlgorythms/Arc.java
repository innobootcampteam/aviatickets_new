package aviatickets.RouteAlgorythms;

import java.util.Date;

import aviatickets.model.Flight;

public class Arc 
{
	private Flight _flight;
	public Arc (Flight flight)
	{
		_flight=flight;
	}

	public Flight getFlight() {
		return _flight;
	}
}
