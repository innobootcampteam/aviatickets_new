package aviatickets.model;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by 1 on 17.07.2016.
 */
public class Reservation {
    private Flight flight;

    public Reservation(Flight flight) {

        this.flight = flight;
    }

    public String getFlightNumber(){
        return flight.getFlightNumber();
    }

    public Airport getAirportFrom() {

        return flight.getAirportFrom();
    }

    public void setAirportFrom(Airport airportFrom) {

        flight.setAirportFrom(airportFrom);
    }

    public Airport getAirportTo() {

        return flight.getAirportTo();
    }

    public void setAirportTo(Airport airportTo) {

        flight.setAirportTo(airportTo);
    }

    public Date getDateTimeFrom() {

        return flight.getDateFrom();
    }

    public void setDateTimeFrom(Date dateTimeFrom) {

        flight.setDateFrom(dateTimeFrom);
    }

    public Date getDateTimeTo() {

        return flight.getDateTo();
    }

    public void setDateTimeTo(Date dateTimeTo) {

        flight.setDateTo(dateTimeTo);
    }

    public Airline getAirline() {

        return flight.getAirline();
    }

    public void setAirline(Airline airline) {

        flight.setAirline(airline);
    }

    public double getPrice() {
        return flight.getPrice();
    }

    public void setPrice(double price) {
        flight.setPrice(price);
    }

    public String getDateTimeString(boolean to){
        DateFormat df = new SimpleDateFormat("dd.MM.yyyy HH:mm");
        if (to){
            return df.format(flight.getDateTo());
        }
        return df.format(flight.getDateFrom());
    }
}
