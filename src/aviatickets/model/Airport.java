package aviatickets.model;

/**
 * Created by 1 on 17.07.2016.
 */
public class Airport {
	
    private String _name;

    public Airport(String name) {
        _name = name;
    }

    public String getName() {
        return _name;
    }

    public void setName(String name) {
        _name = name;
    }
    
}
