/* Created by Natalie on 17.07.2016. */

package aviatickets.model;

public class Airline {

    private String _name;

    public Airline(String name) {
        _name = name;
    }

    public String getName() {
        return _name;
    }
    public void setName(String name) {
        _name = name;
    }

}
