/* Created by Natalie on 17.07.2016. */

package aviatickets.model;

import java.util.Date;

public class Flight
{

    private Airline _airline;
    private int _id;
    private String _flightNumber;

    private Airport _airportFrom;
    private Airport _airportTo;

    private Date _dateFrom;
    private Date _dateTo;

    private double _price;

    private int _freeSeats;

    Flight(){ }
    
    public Flight(Airline airline, int id, String fNumber, Airport aFrom, Airport aTo, Date dateFrom, Date dateTo, double price, int freeSeats){
        _airline = airline;
        _id = id;
    	_flightNumber = fNumber;
        _airportFrom = aFrom;
        _airportTo = aTo;
        _dateFrom = dateFrom;
        _dateTo = dateTo;
        _price = price;
        _freeSeats = freeSeats;
    }
    
    public int getId() {
        return _id;
    }

    public void setId(int id) {
        _id = id;
    }

    public Airport getAirportFrom() {
        return _airportFrom;
    }

    public void setAirportFrom(Airport airportFrom) {
        _airportFrom = airportFrom;
    }

    public Airport getAirportTo() {
        return _airportTo;
    }

    public void setAirportTo(Airport airportTo) {
        _airportTo = airportTo;
    }

    public Date getDateFrom() {
        return _dateFrom;
    }

    public void setDateFrom(Date dateTimeFrom) {
        _dateFrom = dateTimeFrom;
    }

    public Date getDateTo() {
        return _dateTo;
    }

    public void setDateTo(Date dateTimeTo) {
        _dateTo = dateTimeTo;
    }

    public int getFreeSeats() {
        return _freeSeats;
    }

    public void setFreeSeats(int freeSeatsCount) {
        _freeSeats = freeSeatsCount;
    }

    public Airline getAirline() {
        return _airline;
    }

    public void setAirline(Airline airline) {
        _airline = airline;
    }

    public double getPrice() {
        return _price;
    }

    public void setPrice(double price) {
        _price = price;
    }

    public String getFlightNumber() {
        return _flightNumber;
    }

    public void setFlightNumber(String flightNumber) {
        _flightNumber = flightNumber;
    }

    public String toString(){
        String string = _flightNumber+":"
                        +" from "+_airportFrom.getName()+" ("+_dateFrom.toString()+")"+
                        " to "+_airportTo.getName()+" ("+_dateTo.toString()+")"+
                        " for $"+_price+" ["+_freeSeats+" free seats]";
        return string;
    }

	}

