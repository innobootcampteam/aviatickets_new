/* Created by 1 on 17.07.2016. */

package aviatickets.model;

import java.util.ArrayList;

public class Route {

    private ArrayList<Reservation> reservations = new ArrayList<Reservation>();

    public Reservation getReservation(int index) {
        return reservations.get(index);
    }

    public int getReservationsCount(){
        return reservations.size();
    }

    public void addReservation(Reservation reservation){
            reservations.add(reservation);
    }

    public boolean removeReservation(Reservation reservation) {
        return reservations.remove(reservation);
    }

    public String getFromTo(){
        Reservation first = reservations.get(0);
        Reservation last = reservations.get(reservations.size() - 1);
        return first.getAirportFrom().getName() + "-" + last.getAirportTo().getName();
    }

    private Double getTotalCost(){
        Double totalCost = 0.0;
        for (Reservation r :
                reservations) {
            totalCost+=r.getPrice();
        }
        return totalCost;
    }

    public String getDatesAndCost(){
        return getReservation(0).getDateTimeString(false) + " - " + getReservation(reservations.size()-1).getDateTimeString(true) + ", " + getTotalCost();
    }

}
