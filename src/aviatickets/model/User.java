package aviatickets.model;

import java.util.ArrayList;

/**
 * Created by 1 on 17.07.2016.
 */
public class User {
    private String firstName = "";
    private String middleName = "";
    private String lastName = "";
    private String email = "";
    private ArrayList<Ticket> tickets = new ArrayList<>();


    public String getFirstName() {
        return firstName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getEmail() {
        return email;
    }

    public Ticket getTicket(int index) {
        return tickets.get(index);
    }

    public int getTicketsCount(){
        return tickets.size();
    }

    public void addTicket(Ticket ticket){

        tickets.add(ticket);
    }

    /*public boolean removeTicket(Ticket ticket) {
        return tickets.remove(ticket);
    }*/

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public boolean isEverythingAllright(){
        return firstName!=null && firstName!="" &&
                middleName!=null && middleName!="" &&
                lastName!=null && lastName!="" &&
                email!=null && email!="";

    }

    public boolean isPaid() {
        for (int i = 0; i < getTicketsCount(); i++) {
            if (!getTicket(i).isPaid()){
                return false;
            }
        }
        return true;
    }
}
