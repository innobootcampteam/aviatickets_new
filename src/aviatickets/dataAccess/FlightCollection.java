package aviatickets.dataAccess;

import aviatickets.model.Airline;
import aviatickets.model.Airport;
import aviatickets.model.Flight;

import java.util.ArrayList;
import java.util.Date;

public class FlightCollection {

	private static ArrayList<Flight> _flights = new ArrayList<Flight>();

	private static boolean _initialized = false;

	// FlightCollection() { }
	
	public static void addFlight(Flight flight) {
		_flights.add(flight);
	}
	
	public static ArrayList<Flight> searchFlight(boolean fromOrTo, Airport airport, Date dateFrom) {
		// fromOrTo: false is from, true is to

		// limit search
		long day = 24 * 60 * 60 * 1000;
		long days = 2 * day;

		ArrayList<Flight> result = new ArrayList<Flight>();

		for(int i=0; i<_flights.size(); i++){
			Flight tempFlight = _flights.get(i);
            if(tempFlight.getFreeSeats()<=0) { continue; } // skip if no free seats
			Date maxDate = new Date(dateFrom.getTime()+days); // plus 2 days
			if((dateFrom.compareTo(tempFlight.getDateFrom())<=0)&&(maxDate.compareTo(tempFlight.getDateFrom())>=0)){
				if(fromOrTo) { // to
					if(tempFlight.getAirportTo().getName().equals(airport.getName())){
						result.add(tempFlight);
					}
				} else { // from
					if(tempFlight.getAirportFrom().getName().equals(airport.getName())){
						result.add(tempFlight);
					}
				}
			}
		}

		return result;

	}

	public static void init(String path){

		// find all files and create objects
		if(path==null) path = "resources/flights"; // path of resources
		DataSources dss = new DataSources(path);
		dss.findSources();

		// for each source
		for(int i=0; i<dss.getSources().size(); i++){

			// link to source
			DataSource source = dss.getSources().get(i);

			// create airline
			String fileName[] = source.getFile().getName().split("\\.");
			Airline airline = new Airline(fileName[0]);

			// convert lines to objects and add to collection
			StringToFlight converter = new StringToFlight();
			ArrayList<String> flights = source.getLines();
			for(int j=0; j<flights.size(); j++){
				_flights.add(converter.convert(airline, j, flights.get(j)));
			}

		}

		// change state
		_initialized = true;

	}

	public static boolean isInitialized(){
		return _initialized;
	}

	public ArrayList<Flight> getAllFlights(){
		return _flights;
	}

}
