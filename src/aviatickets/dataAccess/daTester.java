/* Created by le5h on 19.07.2016. */

package aviatickets.dataAccess;

import aviatickets.model.Airline;
import aviatickets.model.Flight;

import java.util.ArrayList;
import java.util.Date;
import java.util.Scanner;

public class daTester {

    public static void launch() {

        Scanner scn = new Scanner(System.in);

        System.out.println("Enter test number: ");
        int testNumber = scn.nextInt();

        // test number 1:
        if(testNumber==1){

            Date start = new Date(); // start execution

            int flightsCount = 0;

            // path of resources
            String path = "resources/flights";

            DataSources dsc = new DataSources(path);

            dsc.findSources();

            ArrayList<DataSource> sources = dsc.getSources();

            for(int i=0; i<sources.size(); i++){

                DataSource ds = sources.get(i);

                String fileName[] = ds.getFile().getName().split("\\.");

                Airline airline = new Airline(fileName[0]);

                ArrayList<String> flights = ds.getLines();

                StringToFlight stf = new StringToFlight();

                for(int j=0; j<flights.size(); j++){
                    String line = flights.get(j);
                    System.out.println(line);
                    Flight flight = stf.convert(airline, j, line);
                    System.out.println(flight.toString());
                    flightsCount++;
                }

            }

            Date end = new Date();

            System.out.println("Time spent to parse "+flightsCount+" lines to objects: "+((int)(end.getTime()-start.getTime()))+" ms");

        }

    }

}
