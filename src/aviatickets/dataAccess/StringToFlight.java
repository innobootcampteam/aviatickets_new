package aviatickets.dataAccess;

import aviatickets.model.Airline;
import aviatickets.model.Airport;
import aviatickets.model.Flight;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class StringToFlight {

	// StringToFlight(){ }
	
	public Flight convert(Airline airline, int id, String line) {

		// statements
		String delimiter = "\\|";  // string delimiter
		String dateFormat = "dd.MM.yyyy HH:mm"; // format of input date

		// date parser
		SimpleDateFormat _df = new SimpleDateFormat(dateFormat);

		// split line by delimiter to array
		// 99A33|LGW|DMD|12.08.2016 12:15|12.08.2016 14:15|123.43|20
		String fields[] = line.split(delimiter);
		// TODO: trim each element

		String fNumber = fields[0];

		// airports
		Airport aFrom = AirportCollection.createAirport(fields[1]);
		Airport aTo = AirportCollection.createAirport(fields[2]);

		// dates
		Date dateFrom = new Date();
		Date dateTo = new Date();
		try{
			dateFrom = _df.parse(fields[3]);
			dateTo = _df.parse(fields[4]);
		} catch (ParseException e) { e.printStackTrace(); }

		// price
		double price = Double.parseDouble(fields[5]);

		// free seats
		int freeSeats = Integer.parseInt(fields[6]);

		return new Flight(airline, id, fNumber, aFrom, aTo, dateFrom, dateTo, price, freeSeats);

	}
	
}
