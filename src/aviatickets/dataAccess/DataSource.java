/* Created by le5h on 17.07.2016. */

package aviatickets.dataAccess;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

public class DataSource {

    private String _path; // path to a file
    
    private File _file; // file object

    private Scanner _scn; // scanner object

    private ArrayList<String> _lines; // lines of file
    
    DataSource(String path){

        _path = path;
        
        _file = new File(_path);

        try {
			_scn = new Scanner(_file);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}

        _lines = new ArrayList<String>();
        
        refreshLines();
        
    }

    // lines of file
    public ArrayList<String> getLines(){
        
        return _lines;
        
    }
    
    // read file to lines again
    public void refreshLines(){

    	if(!_lines.isEmpty()){ _lines.clear(); }
    	
        do { _lines.add(_scn.nextLine()); }
        while (_scn.hasNextLine());

        _scn.close();

    }
    
    // update line in file
    public boolean updateFile(String content){

        FileWriter fWriter = null;

        try {
            fWriter = new FileWriter(_file, false);
            fWriter.write(content);
            fWriter.close();
            return true;
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
    	
    }
    
    // return file
    public File getFile(){
    	return _file;
    }

    protected void finalize(){
        _scn.close();
    }
    
}
