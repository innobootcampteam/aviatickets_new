package aviatickets.dataAccess;

import aviatickets.model.Airport;
import java.util.ArrayList;

public class AirportCollection {
	
	private static ArrayList<Airport> _airports = new ArrayList<Airport>();
	
	// AirportCollection() { }
	
	// add new airport if not exists
	public static void addAirport(Airport airport){
		
		if(searchAirport(airport.getName())==null){
			_airports.add(airport);
		}
		
	}

	// create and add airport
	public static Airport createAirport(String name){

		Airport searchFor = searchAirport(name);

		if(searchFor==null){
			Airport newOne = new Airport(name);
			_airports.add(newOne);
			return newOne;
		} else {
			return searchFor;
		}

	}

	// search an airport by name
	public static Airport searchAirport(String name){
		
		for(int i=0; i<_airports.size(); i++){
			if(_airports.get(i).getName().equals(name)){
				return _airports.get(i);
			}
		}
		
		return null;
		
	}
	
	// all airports
	public static ArrayList<Airport> getAirports(){

		return _airports;

	}

}
