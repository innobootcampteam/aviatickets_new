package aviatickets.dataAccess;

import java.io.File;
import java.util.ArrayList;

public class DataSources {

    private String _path;

    private String _ext = ".txt";

    ArrayList<DataSource> _sources;

    DataSources(String path){
        _sources = new ArrayList<DataSource>();
        _path = path;
    }

    public void findSources(){

        File folder = new File(_path);

        File[] files = folder.listFiles();

        for(int i=0; i<files.length; i++){
            if(files[i].isFile() && files[i].getName().indexOf(_ext)>0){
                _sources.add(new DataSource(_path+"/"+files[i].getName()));
            }
        }

    }

    public ArrayList<DataSource> getSources(){
        return _sources;
    }

}
